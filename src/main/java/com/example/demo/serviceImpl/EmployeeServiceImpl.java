package com.example.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Employee;
import com.example.demo.repo.EmployeeRespo;
import com.example.demo.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRespo employeeRespo;

	@Override
	public List<Employee> getAllEmp() {
		
		return employeeRespo.findAll();
	}

	@Override
	public Optional<Employee> getEmpById(Integer id) {
		
		
		
		return employeeRespo.findById(id);
	
		
		
		//return employeeRespo.findById(id);
	}

	@Override
	public void deleteEmpById(Integer id) {
		employeeRespo.deleteById(id);
		
	}

	@Override
	public void saveEmploye(Employee employee) {
		employeeRespo.save(employee);
		
	}

	@Override
	public void updateEmploye(Employee employee) {
		employeeRespo.save(employee);
		
	}

	@Override
	public List<Employee> getEmpByDeptAndSal(String dept, double sal) {
		
		return employeeRespo.getByDeptAndSal(dept, sal);
	}
	

}
