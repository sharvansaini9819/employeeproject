package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.repo.EmployeeRespo;
import com.example.demo.service.EmployeeService;

@RestController
public class EmployeeController {
	
	
	@Autowired
	EmployeeService employeeService;
	
	
	@GetMapping()
	public List<Employee> getAllEmp(){
		return employeeService.getAllEmp();	
	}
	
	
	@GetMapping("{id}")
	public Optional<Employee> getEmpById(@PathVariable("id") Integer id){
		return employeeService.getEmpById(id);	
	}
	
	@DeleteMapping("{id}")
	public String deleteEmpById(@PathVariable("id") Integer id){
		employeeService.deleteEmpById(id);
		 return "emp record with "+id+" is deleted";	
	}
	
	@PostMapping()
	public String saveEmploye(@RequestBody Employee employee) {	
		employeeService.saveEmploye(employee);
		return "emp record save";	
	}
	
	
	@PutMapping()
	public String updateEmploye(@RequestBody Employee employee) {	
		employeeService.updateEmploye(employee);
		return "emp record update";	
	}
	//localhost:8080/dept/ui/sal/55
	@GetMapping("/dept/{dept}/sal/{sal}")
	public List<Employee> getEmpByDeptAndSal(
			@PathVariable("dept") String dept,
			@PathVariable("sal") double sal){
		return employeeService.getEmpByDeptAndSal(dept,sal);	
	}
	
		

}
