package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.Employee;

public interface EmployeeService {

	List<Employee> getAllEmp();

	Optional<Employee> getEmpById(Integer id);

	void deleteEmpById(Integer id);

	void saveEmploye(Employee employee);

	void updateEmploye(Employee employee);

	List<Employee> getEmpByDeptAndSal(String dept, double sal);

}
