package com.example.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Employee;

public interface EmployeeRespo extends JpaRepository<Employee, Integer> {
	
	
	List<Employee> findByDeptAndSal(String dept, double sal);
	
	
	@Query("select a from Employee as a where a.dept=?1 and a.sal=?2")
	List<Employee> getByDeptAndSal(String dept, double sal);
	
	
}
